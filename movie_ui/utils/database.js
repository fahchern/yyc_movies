import mongoose from 'mongoose';

//set to false so we can track the connection status
let isConnected = false;

export const connectToDB = async () => {
    //set up mongoose options:
    mongoose.set('strictQuery', true);

    if(isConnected) {
        console.log('MongoDB is already connected');
        return;
    }

    //if not connected:
    try {
        await mongoose.connect(process.env.MONGODB_URI, {
            dbName: "movie_showings",
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        isConnected = true;

        console.log('MongoDB connected')
    } catch(error){
        console.log(error);
    }
}