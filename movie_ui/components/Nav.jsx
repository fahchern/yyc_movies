"use client"; //directive here for useState and useEffect

import Link from 'next/link'; // so we can link to other pages of our site
import Image from 'next/image'; //auto optimize images
import { useState, useEffect } from 'react'; //these are 'hooks' (client based functionality)
import { signIn, signOut, useSession, getProviders } from 'next-auth/react';

const Nav = () => {
  const isUserLoggedIn = true;

  const [providers, setProviders] = useState (null);

  /* make signIn button open up a menu */
  const [toggleDropdown, setToggleDropdown] = useState (false); 

  {/* For 'Sign In" Set providers: create new use effect hook that has a callback function
"() => {}, []" and only runs at the start*/}
  useEffect(() => {
    const setProviders = async () => {
      const response = await getProviders(); {/*called above*/}

      setProviders(response);
    }
    setProviders();
  }, [])

  return (
    <nav className='flex-between w-full mb-16 pt-3'>
      <Link href='/' className='flex gap-2 flex-center'>
        <Image 
          src="/assets/images/logo.svg"
          alt="Promptopia Logo"
          width={30}
          height={30}
          className='object-contain'
        />
        <p className='logo_text'> 
          Promptopia
        </p>
      </Link>

      {/* Desktop Navigation */}
      <div className='sm:flex hidden'>
        {isUserLoggedIn ? (
          <div className='flex gap-3 md:gap-5'>
            <Link href='/create-prompt' className='black_btn'>
              Create Post
            </Link>

            <button type='button' onClick={signOut} className='outline_btn'>
              Sign Out
            </button>

            <Link href='/profile'>
              <Image
                src='/assets/images/logo.svg'
                width={37}
                height={37}
                className='rounded-full'
                alt='profile'
              />            
            </Link>

          </div>
        ): (
          /* The following is a dynamic block of code */
          <>
            {/*1. Check if we have access to providers
              2. If yes, show all different providers and have a button 
              for each of them for sign-in auth
              3. We will only use one provider - googauth*/}
            {providers && Object.values(providers).map((provider) => (
              <button
                type='button'
                key={provider.name}
                onClick={() => signIn(provider.id)}
                className='black_btn'
              >
                Sign In
                
              </button>
            ))}  
          </>
        )}
      </div>

      {/* Mobile Navigation */}
      <div className='sm:hidden flex relative'>
        {isUserLoggedIn ? (
          <div className='flex'>
            <Image
              src='/assets/images/logo.svg'
              width={37}
              height={37}
              className='rounded-full'
              alt='profile'
              /* Set toggleDropDown to opposite of its current value */
              onClick={() => setToggleDropdown((prevState) => !prevState)} 
            />
            {/*If toggleDropdown is true, the we can render div */}
            {toggleDropdown && (
              <div className='dropdown'>
                <Link
                  href='/profile'
                  className='dropdown_link'
                  onClick={() => setToggleDropdown (false)}
                >
                  My Profile
                </Link>
                <Link
                  href='/create-prompt'
                  className='dropdown_link'
                  onClick={() => setToggleDropdown (false)}
                >
                  Create Prompt
                </Link>
                <button 
                  type='button'
                  onClick={() => {
                    setToggleDropdown(false);
                    signOut();
                  }}
                  className='mt-5 w-full black_btn'
                >
                  Sign Out
                </button>
              </div>
            )}

          </div>
        ): (
          <>
            {providers && Object.values(providers).map((Provider) => (
              <button 
                type='button'
                key={provider.name}
                onClick={() => signIn(provider.id)}
                className='black_btn'
              >
                Sign In
              </button>
            ))}
          </>
        )}

      </div>
      

    </nav>
  )
}

export default Nav