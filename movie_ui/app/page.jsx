// import React from 'react'
// This no longer needs to be specified
//underscore classNames come from our own styling

import Feed from '@components/Feed';

const Home = () => {
  return (
    <section className="w-full flex-center flex-col">
        <h1 className="head_text text-center">
            C-Movies:
            <p className='max-md:hidden' />

        </h1>
        <h1 className='head_text text-center'>    
            <span className="orange_gradient text-center 'max-md:hiddden'"> Movie Lovers Unite! </span>
        </h1>
        <p className="desc text-center">
            Find all the movies playing in theatres in and around Calgary
        </p>

        <Feed />

    </section>
  )
}

export default Home