import '@styles/globals.css';

import Nav from '@components/Nav';
import Provider from '@components/Provider';

// above will import css to entire application

export const metadata = {
    title: "C-Movies",
    description: "Discover all the movies currently in theatres in and around the Calgary area"
}

const RootLayout = ({children}) => {
  return (
    <html lang="en">
        <body>
            {/* All Providers go here, so it can be used across the app
            So Provider component wrap done here */}
            <Provider>

                <div className='main'>
                    <div className='gradient' />
                </div>

                <main className='app'>
                    <Nav />
                    {children}
                </main>
     
            </Provider>
        </body>
    </html>
  )
}

export default RootLayout;