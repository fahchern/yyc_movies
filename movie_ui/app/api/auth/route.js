/* Here we have an api route with an api body, auth, [dynamic nextauth] 
with a route */

import NextAuth from "next-auth";
import GoogleProvider from '.next-auth/providers/google';

import User from '@models/user'
import { connectToDB } from "@utils/database";

const handler = NextAuth({
    providers: [
        GoogleProvider({
            clientId: process.env.GOOGLE_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        })
    ],
    /* To get the session going, you have to first Sign In the user */
    async session({ session }) {
        const sessionUser = await User.findOne({
            email: session.user.email
        })

        session.user.id = sessionUser._id.toString();

        return session;
    },
    async signIn({ profile }) {
        try {
            //serverless route -> lambda func -> dynomodb
            await connectToDB();

            //check if a user already exists
            const userExists = await User.findOne({
                email: profile.email,
            });

            //if not, create a new user
            if(!userExists) {
                await User.create({
                    email: profile.email, 
                    username: profile.name.replace(" ", "").toLowerCase(),
                    image: profile.picture
                })
            }

            //once we sign in
            return true;

        } catch(error) {
            return false;
        }
    }
})
{/* Have to use Get and POST for NextAuth applications
    normally it's either GET or POST */}
export { handler as GET, handler as POST };

/* TIME STAMP: 1HR 37MIN */