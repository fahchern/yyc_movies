import { Schema, model, models } from "mongoose";

const UserSchema = newSchema({
    email: {
        type: String,
        unique: [true, 'Email already exists!'],
        required: [true, 'Email is required'],
    },
    username: {
         type: String,
         required:[true, 'Username is required!'],
         //see if we can get a pw regex that handles complex and long passwords
         match: [/^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/, "Username invalid, it should contain 8-20 alphanumeric letters and be unique!"]
    },
    image: {
        type: String,
    }
})

//additional check because connection is always made from scratch
//So, first check if user exists, if not, then create new model
const User = models.User || model("User", UserSchema);

export default User;

//The above user check is because: 
/**
/* The 'models' object is provided by the Mongoose library and stores
all the registered models. 
/* If a modle named "User" already exists in the "models" object, it
assignes that existing mofel to the "User" variable 
/* This prevents redefining the model and ensures that the existing 
model is resused

/* If a model named "User" does not exist in the "models" object, the
"model" function from the Mongoose is called to create a new model.
/* The newly created model is then assigned to the "User" variable 
**/