from selenium import webdriver
import requests
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import re
from selenium.webdriver.remote.webelement import WebElement

import time

import movie_db


import pandas as pd


def scrape_landmark(url):
    # dictionary for each movie
    # keys = ['Movie Title', 'Release Date', 'Movie Poster', 'Synopsis', 'Language', 'Rotten Tomatoes', 'Maturity Rating', 'Runtime', 'Genre']
    movie_info = {}

    # instantiate browser driver
    browser = webdriver.Firefox()
    # load web page
    browser.get(url)

    # Locate the dropdown element for location selection
    dropdown = Select(browser.find_element(By.ID, 'Location'))
    # Select an option by value for 'All Locations'
    dropdown.select_by_value('/now-playing?view=all')

    time.sleep(2)
    # change sleep to: sleep until finished rendering page

    # Number of movies
    content = browser.find_element(By.ID, 'main')
    content = content.find_element(By.CLASS_NAME, 'contain')
    content = content.find_element(By.CLASS_NAME, 'content')
    content = content.find_element(By.ID, 'showtimesnowbooking')
    movies = content.find_element(By.ID, 'showtimeslist')
    movies = movies.find_elements(By.CLASS_NAME, 'movieitem')
    print(f"Number of Movies: {len(movies)}")

    for info in movies:
        movie_poster = info.find_element(By.TAG_NAME, "img").get_attribute("src")
        movie_info['Movie Poster'] = movie_poster
        # print(movie_info['Movie Poster'])

        title = info.find_element(By.CLASS_NAME, 'filmListItemTitle')
        movie_title = title.get_property('textContent')
        movie_info['Movie Title'] = movie_title
        # print(movie_info['Movie Title'])

        release_date = info.get_property('textContent')
        release_date = release_date.strip().split()
        release_date = release_date[-1]
        movie_info['Theatre Release Date'] = release_date

        # use this var to compare years to get correct url in rotten tomatoes###########################################
        landmark_release_year = release_date.split('/')[2]
        # print(f"landmark release year: {landmark_release_year}")
        # print((f"landmark_release_year: {landmark_release_year}"))





        # get rotten tomato movie info and add to movie_info dictionary
        # ##########################################################Replacing Rotten Tomatoes with movies DB
        # rotten_tomatoes(movie_info, landmark_release_year)

        # for key in movie_info:
        #     print(movie_info[key])


# remember to credit rotten tomatoes for information
def rotten_tomatoes(movie_info_dic, landmark_year):

    # instantiate browser driver
    rt = webdriver.Firefox()

    movie_title = format_title(movie_info_dic['Movie Title'])

    print(f"from rottem tomatoes def:  {movie_title}")

    # load web page
    rt.get(f"https://www.rottentomatoes.com/m/{movie_title}")

    # ########### check here if theatre release year == year on rotten tomatoes
    # ########### if not equal, add '_<year>'
    # ########### in parser, check if you have '_,the'
    # ########### if so then put 'the_' in front of movie
    # eliminate recurring titles
    # ? what to do about other language titles

    # get rotten tomatoes movie year
    rt_year = get_movie_year(rt)
    movie_info_dic['RT Movie Year'] = rt_year
    # print(f"RT movie year: {rt_year}")

    # get new url if years don't match
    if rt_year != landmark_year:
        movie_title = movie_title + '_' + landmark_year
        print(f"new movie title: {movie_title}")
        rt.get(f"https://www.rottentomatoes.com/m/{movie_title}")

    # if movie_info_dic[]

    # get synopsis of movie
    synopsis = get_movie_synopsis(rt)
    movie_info_dic['Synopsis'] = synopsis

    # get tomatometer
    movie_tomatometer = get_movie_tomatometer(rt)
    movie_info_dic['Rotten Tomatoes'] = movie_tomatometer

    # get all items from movie info section
    get_movie_info(rt, movie_info_dic)

    print(f"myDictionary: {movie_info_dic}")

    return movie_info_dic


def get_movie_year(rotten_tomatoes_movie):
    get_year = rotten_tomatoes_movie.find_element(By.ID, 'movie-overview')
    get_year = get_year.find_element(By.ID, 'main')
    get_year = get_year.find_element(By.ID, 'topSection')
    get_year = get_year.find_element(By.ID, 'scoreboard')
    get_year = get_year.find_element(By.TAG_NAME, 'p')
    get_year = get_year.get_attribute('textContent')
    year = get_year.split(',')
    year = year[0]
    return year


def get_movie_info(rotten_tomatoes_movie, movie_dic):
    get_info_list = rotten_tomatoes_movie.find_element(By.ID, 'movie-overview')
    get_info_list = get_info_list.find_element(By.ID, 'main')
    get_info_list = get_info_list.find_element(By.ID, 'movie-info')
    get_info_list = get_info_list.find_element(By.CLASS_NAME, 'panel-body')
    get_info_list = get_info_list.find_element(By.ID, 'info')
    # ###### All movie info list items #############
    all_list_items = get_info_list.find_elements(By.TAG_NAME, 'li')

    for item in all_list_items:
        item = item.get_attribute('textContent')
        item_key_value = parse_movie_item(item)
        movie_dic[item_key_value[0]] = item_key_value[1]

    # print(f"aldksjfa;lj {movie_dic}")

    return movie_dic


# make key value pair for each item
def parse_movie_item(item):
    item = re.sub(' +', ' ', item)
    item = item.strip()
    key = item.split(':')[0]
    val = item.split(':')[1]

    val = re.sub('\n', ' ', val)
    val = re.sub(', +', ' ', val)
    val = re.sub('\xa0.*$', '', val)
    val = val.strip()
    parsed_item = (key, val)
    # print(parsed_item)

    return parsed_item


def get_movie_tomatometer(rotten_tomatoes_movie):
    # movie rating
    get_tomatometer = rotten_tomatoes_movie.find_element(By.ID, 'movie-overview')
    get_tomatometer = get_tomatometer.find_element(By.ID, 'main')
    get_tomatometer = get_tomatometer.find_element(By.ID, 'topSection')
    get_tomatometer = get_tomatometer.find_element(By.ID, 'scoreboard')
    tomatometer = get_tomatometer.get_attribute('tomatometerscore')
    # change to attr

    return tomatometer


def get_movie_synopsis(rotten_tomatoes_movie):
    # movie synopsis
    get_synopsis = rotten_tomatoes_movie.find_element(By.ID, 'movie-overview')
    get_synopsis = get_synopsis.find_element(By.ID, 'main')
    get_synopsis = get_synopsis.find_element(By.ID, 'movie-info')
    get_synopsis = get_synopsis.find_element(By.CLASS_NAME, 'panel-body')
    get_synopsis = get_synopsis.find_element(By.TAG_NAME, 'p')
    synopsis = get_synopsis.get_property('textContent')
    format_synopsis = synopsis.strip()

    return format_synopsis

def format_title(movie_name):
    format_movie_title = movie_name.lower()
    regex_remove_hyphen = "-.*$"
    format_movie_title = re.sub(regex_remove_hyphen, "", format_movie_title)
    regex_remove_parenthesis = '\(([^)]+)\).*$'
    format_movie_title = re.sub(regex_remove_parenthesis, "", format_movie_title)
    regex_remove_trailing_whitespace = '[\s]+$'
    format_movie_title = re.sub(regex_remove_trailing_whitespace, "", format_movie_title)
    # format_movie_title.strip()
    print(f"format1: {format_movie_title}")

    # # the following removes ', the' from movie titles and adds it to the front of the string
    # if ',' in format_movie_title:
    #     new_title = ',.*$'
    #     format_movie_title = re.sub(new_title, "", format_movie_title)
    #     format_movie_title = 'the ' + format_movie_title
    #     print(f"format2: {format_movie_title}")

    # movie_title = format_movie_title.replace(" ", "_")
    # movie_title = movie_title.replace("'", "")
    movie_title = format_movie_title.strip()
    print(f"format3: {movie_title}")

    # if movie_title == "le_souhait:_asha_et_la_bonne_étoile":
    #     movie_title = "wish_2023"
    # if movie_title == "tiger_3":
    #     movie_title = "tiger_3_2023"
    # if movie_title == "be_my_family":
    #     movie_title = "wu_jin_jia_zu"
    # if movie_title == "hunger_games:_ballad_of_songbirds_&_snakes":
    #     movie_title = "the_hunger_games_the_ballad_of_songbirds_and_snakes"

    return movie_title