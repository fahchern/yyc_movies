class MovieInfo:
    def __init__(self, name: str, poster, language: str, run_time: str, release_date: int, genre: [str],
                 maturity_rating: str, synopsis: str, trailer):
        # name
        self.name = name
        # movie jpg image poster
        self.poster = poster
        # string representing original movie language
        self.language = language
        # string in hr:min format of how long movie runs for
        self.run_time = run_time
        # DD/MM/YY format integer
        self.release_date = release_date
        # list of strings defining movie genre
        self.genre = genre
        # string for maturity rating ?format
        self.maturity_rating = maturity_rating
        # string
        self.synopsis = synopsis
        # link to trailer
        self.trailer = trailer

