import landmark_scrape
import movie_db

# what we need to do here is to loop over each movie from landmark
# NEED TO GET SHOWTIMES and 
# get the movie_db information
# then print them out

'''
Theatres that need scraping:
- The Plaza
- Globe Cinema
- Infinity Dome
- Canyon Medows
- Cochrane Movie House
- Okotokes Cinemas
'''



if __name__ == '__main__':
    # format_title('Napoleon - The Imax Experience')
    # scrape_landmark(f"https://www.landmarkcinemas.com/now-playing/")
    # sample_info = {'Movie Title': 'Holdovers, The'}
    # rotten_tomatoes(sample_info)
    # movie_db.get_movie_db("napoleon", "2023")

    mdb = movie_db.MovieDB()
    mdb.fetch_genres() # call me once
    mdb.fetch_languages()
    mdb.fetch_movie_certifications()
    mdb.fetch_img_config_info()
    mdb.get_movie_info("napoleon", 2023)

