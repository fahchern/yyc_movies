import creds
import requests
import datetime
import movie_information as movie_info_dic
import json


class MovieDB:
    def __init__(self):

        self.genre_cache = {}

        # language_cache has 2 values for id(key)
        # format: {'az': ['Azerbaijani', 'Azərbaycan'], 'fr': ['French', 'Français'],...}
        # az[0] -> has a guaranteed value
        self.language_cache = {}

        self.maturity_rating_cache = {}

        self.headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {creds.access_token_api_key}"
        }

        self.secure_img_base_url = ""

    def fetch_genres(self):
        genre_info = f"https://api.themoviedb.org/3/genre/movie/list?language=en"

        genre_response = requests.get(genre_info, headers=self.headers)
        genre_objects = genre_response.json()
        genre_objects = genre_objects['genres']

        genre_dic = {}
        for obj in genre_objects:
            genre_id = obj['id']
            genre_name = obj['name']
            genre_dic[genre_id] = genre_name

        self.genre_cache = genre_dic

    def fetch_languages(self):
        language_info = f"https://api.themoviedb.org/3/configuration/languages"

        language_response = requests.get(language_info, headers=self.headers)
        language_objects = language_response.json()

        language_dic = {}
        for obj in language_objects:
            language_id = obj['iso_639_1']
            language_name = obj['english_name']
            language_og_name = obj['name']
            language_dic[language_id] = [language_name, language_og_name]

        self.language_cache = language_dic
        # print(f"LANG = {language_dic}")

    def fetch_movie_certifications(self):
        maturity_rating_info = "https://api.themoviedb.org/3/certification/movie/list"

        maturity_rating_response = requests.get(maturity_rating_info, headers=self.headers)
        maturity_rating_response = maturity_rating_response.json()
        certs = maturity_rating_response['certifications']

        maturity_rating = {}
        if 'CA' in certs:
            maturity_rating = certs['CA']

        self.maturity_rating_cache = maturity_rating

    def fetch_img_config_info(self):
        # configuration of posters and backdrops
        image_configuration_info = "https://api.themoviedb.org/3/configuration"

        img_config_info = requests.get(image_configuration_info, headers=self.headers)

        print(f"imgINfo: {img_config_info.text}")
        images = img_config_info.json()
        secure_img_base_url = images['images']['secure_base_url']
        self.secure_img_base_url = secure_img_base_url
        print(f"images: {secure_img_base_url}")

    def get_movie_info(self, movie_title, release_year):
        movie_info = (
            f"https://api.themoviedb.org/3/search/movie?query={movie_title}&include_adult=false&language=en-US"
            f"&primary_release_year={release_year}page=1"
        )

        # movie info: list of dictionaries
        # looks like movie_res = [{movie1 info}, {movie2 info}, ... {movieX info}]
        movie_response = requests.get(movie_info, headers=self.headers)
        movie_information = movie_response.json()
        movie_res = movie_information['results']
        # print(f"Read1: {movie_res}")

        # match correct movie information to movie title
        for movie_obj in movie_res:
            res_title = movie_obj['original_title'].lower()
            # print(f"movie title: {movie_title}")
            # print(f"res title: {res_title}")
            if movie_title == res_title:
                my_movie = movie_obj
                print(f"My MOvie: {my_movie}")

        # movie title ##############################################################
        my_movie_title = my_movie['original_title']
        # print(f"my_movie_title = {my_movie_title}")

        # movie language ###########################################################
        my_movie_lang = my_movie['original_language']
        for lang_id in self.language_cache:
            if my_movie_lang in self.language_cache:
                my_movie_language = self.language_cache[my_movie_lang][0]
        # print(f"my LANGUAGE = {my_movie_language}")

        # movie genre(s) ###########################################################
        # find genre names from given id's
        movie_genre_ids = my_movie['genre_ids']
        # print(f"My Movie Genre ids: {movie_genre_ids}")

        # print(f"Genre Cache: {self.genre_cache}")
        my_movie_genre = []
        for movie_ids in movie_genre_ids:
            if movie_ids in self.genre_cache:
                my_movie_genre.append(self.genre_cache[movie_ids])
        # print(f"NEW ONE {my_movie['original_title']} Genres: {my_movie_genre}")

        # movie synopsis ##########################################################
        my_movie_synopsis = my_movie['overview']
        # print(f"my_movie_synopsis: {my_movie_synopsis}")

        # release date ############################################################
        my_movie_release_date = my_movie['release_date']
        # print(f"my_movie_release_date: {my_movie_release_date}")
        to_string = datetime.datetime.strptime(my_movie_release_date, '%Y-%m-%d')
        my_movie_release_date = to_string.strftime('%B %d, %Y')
        # print(f"my_movie_release_date: {my_movie_release_date}")

        # maturity rating #########################################################
        # get movie id
        my_movie_id = my_movie['id']
        # print(f"movie id: {my_movie_id}")

        # get result of release dates and certifications(maturity rating) for movie using id
        release_dates = "https://api.themoviedb.org/3/movie/753342/release_dates"
        release_dates = requests.get(release_dates, headers=self.headers)
        release_dates = release_dates.json()
        results = release_dates['results']
        # print(f"release_dates: {results}")

        # iso_ id CA has certification - ie maturity rating
        for obj in results:
            if obj['iso_3166_1'] == "CA":
                cert = obj['release_dates']
                for i in cert:
                    if i['certification']:
                        my_movie_rating = i['certification']
                        # print(f"my_movie_rating: {my_movie_rating}")

        # compare to list of certs to and get meaning of movie_rating
        for cert in self.maturity_rating_cache:
            if cert['certification'] == my_movie_rating:
                cert_meaning = cert['meaning']
                my_movie_rating += ": " + cert_meaning
                # print(my_movie_rating)

        # movie runtime #############################################################
        my_movie_details = "https://api.themoviedb.org/3/movie/753342?language=en-US"

        # !!!!!!!!!!!!!!! Note: details also has posters, backdrop, imdb_id, release date,
        # original language, popularity, movie website
        my_movie_details = requests.get(my_movie_details, headers=self.headers)
        my_movie_details = my_movie_details.json()
        my_movie_runtime = str(my_movie_details['runtime'])
        my_movie_runtime += ' min'

        print(f"Runtime: {my_movie_runtime}")

        # poster ####################################################################
        my_movie_poster = my_movie_details['poster_path']
        my_movie_backdrop = my_movie_details['backdrop_path']

        print(f"poster: {my_movie_poster}")
        print(f"backdrop: {my_movie_backdrop}")
        my_img = self.secure_img_base_url + "w500" + my_movie_poster
        print(my_img)

        # need to find the trailer for the movies
        # movie trailer #############################################################
        my_movie_trailer = "https://api.themoviedb.org/3/movie/753342/videos?language=en-US"

        response = requests.get(my_movie_trailer, headers=self.headers)
        print("trailer")
        res = response.json()
        res = res['results']
        for trailer in res:
            if trailer['name'] == 'Official Trailer':
                official_trailer_key = trailer['key']
            else:
                official_trailer_key = trailer['key']
        my_movie_trailer = f"https://www.youtube.com/watch?v={official_trailer_key}"
        print(my_movie_trailer)

# then we can return the movei_information instance
        movie_info_dic.MovieInfo(my_movie_title, my_img, my_movie_language, my_movie_runtime, my_movie_release_date,
                                 my_movie_genre, my_movie_rating, my_movie_synopsis, my_movie_trailer)

# ############################# Return an instance of the movie_information.py with this definition

#  all the above info should be in its own def.
#  the above must be refactored
